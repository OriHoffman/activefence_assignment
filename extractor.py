from googleapiclient import discovery
import utils


class Extractor:

    def __init__(self, version='v3'):
        # Arguments that need to passed to the build function
        self.DEV_KEY = "AIzaSyBUt7dFDp63KcOMflf7ksZOYWcbiwxH1og"
        self.SERVICE_NAME = "youtube"
        self.VERSION = version

        # creating Youtube Resource Object
        self.client = discovery.build(self.SERVICE_NAME,
                                      self.VERSION,
                                      developerKey=self.DEV_KEY)

    def fetch_comments(self, videoId, amount):
        comments = list()
        req = utils.generate_req(videoId, amount)
        while len(comments) < (amount - (100 % amount)):
            res = req.execute()
            comments.extend(utils.parse_commentThread(res))
            req = self.client.commentThreads().list_next(req, res)
        last_req = utils.change_req_maxResults(
                   self.client.commentThreads().list_next(req, res),
                   amount % 100)
        res = last_req.execute()
        comments.extend(utils.parse_commentThread(res))
        return comments
