from manager import comments_cache
from flask_restful import Resource, Api, reqparse
from extractor import Extractor

extractor = Extractor()

get_parser = reqparse.RequestParser()
get_parser.add_argument('videoId', type=str, location='json', required=True, help='need video ID')
get_parser.add_argument('amount', type=int, help='number of comments to fetch')


class GetComments(Resource):
    def get(self):
        args = get_parser.parse_args()
        try:
            comments_cache.extend(extractor.fetch_comments(args['videoId'], args['amount']))
        except Exception as e:
            print(e)
        return {'status': 200}
