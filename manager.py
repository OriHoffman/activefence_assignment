from pymongo import MongoClient
import time
import logging
from threading import Lock

MAX_TIME_CACHE = 0.5

comments_cache = []


class DBManager:
    _client = MongoClient('localhost:27017')
    _db = _client.activefence
    _lock = Lock()
    _logger = logging.getLogger(__name__)

    @staticmethod
    def dump_to_db():
        """
        Thread which dumps new vectors to DB each MAX_TIME_CACHE seconds or when there are enough vectors
        :return:
        """
        global comments_cache
        start = time.time()

        while True:
            if time.time() - start > MAX_TIME_CACHE:
                try:
                    with DBManager._lock:
                        DBManager._db.comments.insert_many(comments_cache)
                        comments_cache = []
                except Exception as e:
                    DBManager._logger.info(e)
                start = time.time()
            time.sleep(0.05)

