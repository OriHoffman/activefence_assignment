def parse_commentThread(res):
    comments = []
    for thread in res['items']:
        comments.append({
            'id': thread['id'],
            'info': thread['snippet'],
            'replies': thread.get('replies', {'comments': []})
        })
    return comments


def change_req_maxResults(self, req, amount):
    req.uri = req.uri.replace('maxResults=100', 'maxResults='+str(amount))
    return req


def generate_req(self, video_id, amount=10):
    req = self.client.commentThreads().list(
        part='snippet,replies',
        videoId=video_id,
        maxResults=100,
        order='time'
    )
    return req

