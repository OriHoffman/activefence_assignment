from flask import Flask
from flask_restful import Resource, Api, reqparse
from resources import GetComments
from threading import Thread
from manager import DBManager, comments_cache

app = Flask(__name__)
api = Api(app)

api.add_resource(GetComments, '/')

Thread(target=DBManager.dump_to_db, daemon=True).start()  # start posting to db


if __name__ == '__main__':
    app.run(debug=True)
